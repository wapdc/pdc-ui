<script>
/**
 * PAmount Component
 *
 * This component represents an input field that accepts a numeric amount. It supports both positive
 * and negative numbers based on configuration, with validation rules and formatting applied.
 * It emits events for value updates and validation errors, making it suitable for use in forms
 * or any input-driven logic.
 *
 * Props:
 * - modelValue (String | Number, required): The initial value for the input field.
 * - label (String, optional, default: "Amount"): The label for the input field.
 * - allowNegative (Boolean, optional, default: false): Whether negative values are allowed.
 * - rules (Array<Function>, optional, default: []): Custom validation rules provided by the parent component.
 *
 * Events:
 * - update:modelValue: Emits the updated value when the input changes.
 *
 * Internal Behavior:
 * - The input only allows numeric values, a single decimal point, and a single minus sign.
 * - Validation runs on every input change, and a custom error message is emitted if validation fails.
 * - The input is rounded to two decimal places.
 */
export default {
  name: "PAmount",
  inheritAttrs: true
};
</script>

<script setup>
import { ref, computed, watch } from "vue";

/** Emits events for value updates and validation errors */
const emit = defineEmits(["update:modelValue", "validationError"]);

/**
 * Defines component props
 * @typedef {Object} Props
 * @property {string|number} modelValue - Bound model value.
 * @property {string} label - Label for the input field.
 * @property {boolean} allowNegative - Whether negative values are allowed.
 * @property {Array<Function>} rules - Custom validation rules.
 */
const props = defineProps({
  modelValue: { type: [String, Number, null], required: true },
  label: { type: String, default: "Amount" },
  allowNegative: { type: Boolean, default: false },
  rules: { type: Array, default: () => [] },
});

/** Local representation of the input value */
const localValue = ref('');

/**
 * Handle updating of value on blur.
 */
const onBlur = () => {
  // Rounding first ensures that we don't send over fractional cents.
  roundAmount()
  // We only emit value on blur because otherwise we trigger
  // model updates to frequently, which would in turn trigger
  // the resetting of localValue.value
  if (!isNaN(parseFloat(localValue.value))) {
    emit('update:modelValue', localValue.value)
  }
  else {
    emit('update:modelValue', null)
  }
};

/**
 * Computes validation rules based on props and custom rules.
 * @returns {Array<Function>} Array of validation functions.
 */
const validationRules = computed(() => [
  (val) => {
    if (!props.allowNegative && parseFloat(val) < 0) {
      return "Negative amounts are not allowed";
    }
    return true;
  },
  ...props.rules,
]);

/**
 * Rounds the current amount to two decimal places.
 * Emits an update event with the numeric value.
 */
const roundAmount = () => {
  const numericValue = parseFloat(localValue.value);
  if (!isNaN(numericValue)) {
    localValue.value = numericValue.toFixed(2);
  } else {
    localValue.value = '';
  }
};


function setModelValue( newValue) {
  // Convert the incoming new value to a number
  // If it's not a valid number, parseFloat returns NaN
  const newNumericValue = parseFloat(newValue);

  if (!isNaN(newNumericValue)) {
    // This maintains the expected string type for the input field
    localValue.value = newValue ? String(newValue) : '';
  }
  else {
    localValue.value = ''
  }
  roundAmount()
}


watch(() => props.modelValue, (newValue) => {
      setModelValue(newValue)
    }
);

// Initial validation and rounding on component mount
setModelValue(props.modelValue)

</script>

<template>
  <q-input
      v-model.number="localValue"
      :label="label"
      type="text"
      inputmode="decimal"
      placeholder="Enter amount"
      :rules="validationRules"
      prefix="$"
      v-bind="$attrs"
      @blur="onBlur"
  />
</template>
