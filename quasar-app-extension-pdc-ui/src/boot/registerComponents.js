import PApp from "../components/PApp.vue";
import PAccessibleButton from "../components/PAccessibleButton.vue";
import PDatePicker from "../components/PDatePicker.vue";
import PEditRows from "../components/PEditRows.vue";
import PEditTable from "../components/PEditTable.vue";
import PHaveAny from "../components/PHaveAny.vue";
import PInfoDialog from "../components/PInfoDialog.vue";
import PSearchRows from "../components/PSearchRows.vue";
import PSelectSearch from "../components/PSelectSearch.vue";
import PRadioSearch from "../components/PRadioSearch.vue";
import PUserHeader from "../components/PUserHeader.vue";
import PGreenLogo from "../components/PGreenLogo.vue";
import PWhiteLogo from "../components/PWhiteLogo.vue";
import PWizard from "../components/PWizard.vue";
import PUserLayout from "../components/PUserLayout.vue";
import PDraftWatermark from "../components/PDraftWatermark.vue";
import PPhone from "../components/PPhone.vue";
import PAmount from "../components/PAmount.vue";
import PBtn from "../components/PBtn.vue";
import PEmail from "../components/PEmail.vue";

const components = {
  PEmail,
  PBtn,
  PAmount,
  PPhone,
  PApp,
  PAccessibleButton,
  PDatePicker,
  PDraftWatermark,
  PEditRows,
  PEditTable,
  PHaveAny,
  PInfoDialog,
  PSearchRows,
  PSelectSearch,
  PRadioSearch,
  PUserHeader,
  PGreenLogo,
  PWhiteLogo,
  PWizard,
  PUserLayout
}

export default ({app}) => {
  for (const prop in components) {
    if (components.hasOwnProperty(prop)) {
      const component = components[prop]
      app.component(prop, component)
    }
  }
}
