import fs from 'fs';
import path from 'path';
import readline from 'readline';
import { fileURLToPath } from 'url';

// __dirname is not defined in ES modules, so this is a workaround
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const componentsDir = path.join(__dirname, 'src', 'components');
const registerComponentsPath = path.join(__dirname, 'src', 'boot', 'registerComponents.js');

// Create readline interface to capture user input
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Function to ask for the component name
rl.question('Enter a name for the component (without the leading P): ', (name) => {
  if (!name) {
    console.error('Component name cannot be empty.');
    rl.close();
    return;
  }

  const componentName = `P${name}`;
  const componentFile = path.join(componentsDir, `${componentName}.vue`);

  // Create Vue component template
  const componentTemplate = `
<script>
export default {
  name: "${componentName}"
}
</script>

<script setup>
</script>

<template>
</template>
`;

  // Write the new component file
  fs.writeFileSync(componentFile, componentTemplate.trim(), 'utf8');
  console.log(`Component ${componentName}.vue created successfully in the components folder.`);

  // Read the existing registerComponents.js file
  fs.readFile(registerComponentsPath, 'utf8', (err, data) => {
    if (err) {
      console.error(`Error reading registerComponents.js: ${err}`);
      rl.close();
      return;
    }

    // Regex to find the last 'import ...;' statement
    const importRegex = /(import\s+.*?;\s*)/g;
    let match;
    let lastImportPos = -1;

    while ((match = importRegex.exec(data)) !== null) {
      lastImportPos = match.index + match[0].length;
    }

    if (lastImportPos === -1) {
      console.error('No import statements found in the registerComponents.js file.');
      rl.close();
      return;
    }

    // Create import statement for the new component
    const importStatement = `import ${componentName} from "../components/${componentName}.vue";\n`;

    // Insert the new import statement after the last import statement
    const newContent = data.slice(0, lastImportPos - 1) + importStatement + '\n' + data.slice(lastImportPos);

    // Append the component to the components object
    const updatedContent = newContent.replace(
      'const components = {',
      `const components = {\n  ${componentName},`
    );

    // Write updated content back to registerComponents.js
    fs.writeFileSync(registerComponentsPath, updatedContent, 'utf8');
    console.log(`Component ${componentName} added to registerComponents.js successfully.`);

    rl.close();
  });
});
