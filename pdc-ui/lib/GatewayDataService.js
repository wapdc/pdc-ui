import axios from 'axios'
import {Loading, Notify, Dialog} from "quasar"

// Common error handling function
function handleError(error, uiBlocking) {
  console.error(error)
  let notify = true
  let message
  if (error && error.response) {
   const status = error.response.status
   switch (status) {
     case 409:
       notify = false;
       Dialog.create({
         title: 'Conflict detected',
         message: 'This document has been modified by another user since it was loaded.  You will need to reload the document before any changes can be saved.  Are you ready to reload?',
         ok: 'Yes',
         cancel: 'Not yet'
         })
         .onOk(() => {
           window.location.reload();
         })
       break;
     case 403:
       message = 'Access denied: You are not authorized to perform this action.'
       break;
     default:
       message = 'An error occurred while trying to process your request.  Please contact PDC support.'
       break;
   }
  }
  else {
      message =  'We were unable to contact the PDC.  Please check you internet connection and try again later.'
  }
  if (notify) {
    Notify.create({
      position: 'top',
      type: 'negative',
      message
    })
  }
  if (uiBlocking) {
    Loading.hide()
  }
}
export class GatewayDataService {
  init(gatewayUrl, token) {
    this.url = gatewayUrl;
    this.gateway = axios.create({
      baseURL: gatewayUrl,
      headers: {
        "Authorization": token
      }
    })
  }

  async get(path, uiBlocking = true, config = {}) {
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.get(path, config)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }catch(error){
      handleError(error, uiBlocking)
      return null
    }
  }

  async post(path, param_data, uiBlocking = true){
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.post(path, param_data)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }
    catch (error) {
      handleError(error, uiBlocking)
      return {}
    }
  }

  async put(path, param_data, uiBlocking = true){
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.put(path, param_data)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }
    catch (error) {
      handleError(error, uiBlocking)
      return {}
    }
  }

  async delete(path, uiBlocking = true) {
    if(uiBlocking){
      Loading.show()
    }
    let success = true
    try{
      await this.gateway.delete(path)
      Loading.hide();
    }catch (error){
      success = false
      handleError(error, uiBlocking)
    }
    return success
  }

}