import axios from 'axios'
import {Loading, Notify} from "quasar"
export class ApolloDataService {
  init(apolloUrl) {
    this.url = apolloUrl;
    this.gateway = axios.create({
      baseURL: apolloUrl,
      withCredentials: true,
    })
  }

  async get(path, uiBlocking = true, config = {}) {
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.get(path, config)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }catch(error){
      Notify.create({
        position: "top",
        type: "negative",
        message: "Request was unsuccessful: " + error.message,
      })
      console.error(error.message)
      if (uiBlocking) {
        Loading.hide()
      }
      return null
    }
  }

  async post(path, param_data, uiBlocking = true){
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.post(path, param_data)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }
    catch (error) {
      const error_message = (error?.response?.data?.message) ? error.response.data.message : error.message
      Notify.create({
        position: 'top',
        type: 'negative',
        message: 'Request was unsuccessful: ' + error_message,
      })
      console.error(error.message)
      if (uiBlocking) {
        Loading.hide()
      }
      return {}
    }
  }

  async put(path, param_data, uiBlocking = true){
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.put(path, param_data)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }
    catch (error) {
      const error_message = (error?.response?.data?.message) ? error.response.data.message : error.message
      Notify.create({
        position: 'top',
        type: 'negative',
        message: 'Request was unsuccessful: ' + error_message,
      })
      console.error(error.message)
      if (uiBlocking) {
        Loading.hide()
      }
      return {}
    }
  }

  async delete(path, uiBlocking = true) {
    if(uiBlocking){
      Loading.show()
    }
    let success = true
    try{
      await this.gateway.delete(path)
      Loading.hide();
    }catch (error){
      success = false
      Notify.create({
        position: 'top',
        type: 'negative',
        message: 'Request was unsuccessful: ' + error.message,
      })
      console.error(error.message)
      if(uiBlocking){
        Loading.hide()
      }
    }
    return success
  }

}