const validStateFields = [
    "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC",
    "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA",
    "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",
    "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC",
    "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"
  ]
export default {

  states: validStateFields,

  // Validates that the value is present
  required: value => !!value || 'Required',

  // Validates that the value is a boolean
  requireBoolean: (value) => value === true || value === false || 'Required',

  // Validates that the value is a non-empty string
  requireString: value => !(!value || !value.trim()) || 'Required',

  // Validates that the amount is positive and adheres to specific formatting rules
  positiveAmount: value => {
    // determine the maximum possible number for a numeric(16,2)
    const maxNumber = 99999999999999.99;

    // when v is not a valid decimal number
    if (isNaN(value)) return 'Invalid number'

    // convert the number to a float
    const floatNum = parseFloat(value)

    if (floatNum > maxNumber) return 'Invalid amount'
    if (floatNum.toString().split(".")[1]?.length > 2) return 'Dollar amounts cannot contain fractional cents'
    if (floatNum === 0) return 'Amount must be greater than 0'

    return !value || parseFloat(value) >= 0 || 'Negative amounts are not allowed'
  },

  // Validates the format and length of an email address
  validEmail: value => {

    // Regular expression for validating email addresses
    const emailRegex = new RegExp(
      // Local-part
      `^(?:(?:[a-zA-Z0-9!#$%&'*+/=?^_\`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_\`{|}~-]+)*)|(?:\\"(?:\\\\[\\x00-\\x7F]|[^\\\\"])*\\"))@` +
      // Domain
      `(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,}|` +
      // Literal IP address
      `\\[(?:(?:IPv6:[a-fA-F0-9:.]+)|(?:\\d{1,3}\\.){3}\\d{1,3})\\])$`
    );

    // Check overall length
    if (value.length > 254) {
      return 'Email must not exceed 254 characters.';
    }

    // Perform regex test
    if (!emailRegex.test(value)) {
      return 'Please enter a valid email address.';
    }

    const [localPart, domain] = value.split('@');

    // Validate local-part length
    if (localPart.length > 64) {
      return 'The local part of the email must not exceed 64 characters.';
    }

    // Validate domain length
    if (domain.length > 255) {
      return 'The domain part of the email must not exceed 255 characters.';
    }

    // Validate each domain label length
    const domainLabels = domain.match(/[^.]+/g) || [];
    for (const label of domainLabels) {
      if (label.length > 63) {
        return 'Each label in the domain must not exceed 63 characters.';
      }
    }

    return true;
  },

  // Validates that the certification statement is exactly 'I CERTIFY'
  validCertificationStatement: value => (value && value.toUpperCase().trim() === 'I CERTIFY') || "Invalid certification",

  // 7 is the statute of limitations, don't accept reports that older than 7 years or in the future by 7 years
  reasonableReportingYear: value => {
    return (+value <= (new Date()).getFullYear() + 7 && +value >= (new Date()).getFullYear() - 7) ||
      `Year must be ${(new Date()).getFullYear() - 7} - ${(new Date()).getFullYear() + 7}`
  },

  // Validates a phone number based on the provided mask.
  validatePhoneNumber: (value, mask) => {
    // Remove any non-digit characters from the value for comparison
    const cleanValue = value.replace(/\D/g, '');

    // Count the number of digits required by the mask
    const requiredDigits = (mask.match(/#/g) || []).length;

    // Check if the cleaned value has the correct number of digits
    if (cleanValue.length !== requiredDigits) {
      return `Phone number must have ${requiredDigits} digits`;
    }

    // Validate against the mask format
    let maskIndex = 0;
    let valueIndex = 0;

    while (maskIndex < mask.length && valueIndex < cleanValue.length) {
      if (mask[maskIndex] === '#') {
        // Only digit input is expected
        if (!/\d/.test(cleanValue[valueIndex])) {
          return 'Invalid phone number format';
        }
        valueIndex++;
      }
      maskIndex++;
    }

    return true; // Validation passed
  },

  // Validates the amount format, optionally allowing negative values
  validateAmount: (value) => {
    const regex = new RegExp(`^${allowNegative ? '-?' : ''}\\d+(\\.\\d{2})?$`);
    const cleanedValue = value.replace(/[^0-9.-]/g, '');
    return regex.test(cleanedValue) || 'Invalid amount format';
  },

  validState: function (value) {
    // Check if the state is 2 characters long
    if ((value || '').length !== 2) return 'State code must be 2 characters only';

    // Verify the state exists in the list
    return validStateFields.includes(value.toUpperCase()) || 'Invalid state';
  },

  address: {
    // Validates that the ZIP code is in the correct format
    validZipCode: value => {
      if (!value) return 'ZIP code is required.';
      // US ZIP code regex: 5 digits or 5 digits + hyphen + 4 digits
      const zipRegex = /^\d{5}(-\d{4})?$/;
      return zipRegex.test(value) || 'Invalid ZIP code format.';
    },

    // Validates that the city contains only letters and hyphens
    validCity: value => {
      if (!value) return 'City is required.';
      const cityRegex = /^[a-zA-Z-]+$/;
      return cityRegex.test(value) || 'City can only contain letters and hyphens.';
    },

    // Validates that the country contains only letters and hyphens
    validCountry: value => {
      if (!value) return 'Country is required.';
      const countryRegex = /^[a-zA-Z-]+$/;
      return countryRegex.test(value) || 'Country can only contain letters and hyphens.';
    },

    // Validates that the street address is provided and does not exceed 100 characters
    validStreet: value => {
      if (!value) return 'Street address is required.';
      if (value.length > 100) return 'Street address must not exceed 100 characters.';
      return true;
    },

    /**
     * Wraps a validator to make the field optional.
     * If the value is not provided, it passes validation.
     * Otherwise, it applies the provided validator.
     * 
     * @param {Function} validator - The validator function to apply if the value is present.
     * @returns {Function} - A new validator function.
     */
    optional: (validator) => {
      return (value) => {
        if (!value) return true; // Field is optional
        return validator(value);
      };
    },
  }

};
