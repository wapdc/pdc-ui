/**
 * Application level information store.
 *
 * When using, be sure to await the useApplication() call so that the appInfo and user properties are set.
 */
import {reactive} from "vue";
import {GatewayDataService} from "./GatewayDataService.js"

import axios from 'axios'
import {ApolloDataService} from "./ApolloDataService.js";
let service_host
let apollo_url
const devMode = (location.host.search('localhost') >= 0) || (location.host.search('127.0.0.1') >= 0) ||
  (location.host.search('lndo.site') >=0) || (location.host.search('ddev.site') >= 0)

if (devMode) {
  // Set up urls when running using npm run build
  apollo_url = window.localStorage.apollo_url ?? 'https://apollod8.lndo.site'
  service_host = apollo_url + '/'
} else {
  // Assume that the site that hosts the application is also the site all service urls and redirects come from.
  service_host = '/'
  apollo_url = location.protocol + "//" + location.host
}

const apollo = axios.create({
  baseURL: service_host,
  withCredentials: true
})

const user = reactive({})

const gateways = {};
const apolloServices = {};

export const useApplication = async function() {

  if (Object.keys(user).length === 0) {
    await refresh()
  }
  return {user, hasAccessToContent, userHasRight, userHasPermission, getSetting, refresh}
}

function getSetting(property){
  if (property === 'apollo_url') {
    return apollo_url
  }
  if (user.settings && user.settings[property]) {
    return  user.settings[property]
  }
  if (property === '')
  return null
}

export async function useGateway(gatewayName, devPort) {
  let gateway;
  if (!gateways[gatewayName]) {
    gateways[gatewayName] = new GatewayDataService()
    gateway = gateways[gatewayName]

    // Figure out the url from wapdc_settting
    let gatewayUrl
    if (user.settings && user.settings[gatewayName]) {
      gatewayUrl = user.settings[gatewayName]
    }
    else if (devMode) {
      gatewayUrl = 'http://127.0.0.1:' + devPort
    }
    else {
      throw new Error('Gateway ' + gatewayName + ' not configured')
    }

    await gateway.init(gatewayUrl, user?.auth?.token ?? "")
    return gateway
  }
  else {
    return gateways[gatewayName]
  }

}

export function useApollo(path) {
  let service;
  if (!apolloServices[path]) {
    apolloServices[path] = new ApolloDataService()
    service = apolloServices[path]
    service.init(service_host + path)
    return service
  }
  else {
    return apolloServices[path]
  }
}

function refreshGatewayTokens(token) {
  for( const gatewayName in gateways) {
    gateways[gatewayName].init(gateways[gatewayName].url, token)
  }
}

async function refresh() {
  let data
  try {
    data = (await apollo.get('public/user-info')).data
  }
  catch (e) {
    if (!devMode) {
      throw "Error retrieving credentials"
    }
    else {
      // This enables developing without apollo running.
      data = {
        name: "developer",
        uid: "-1",
        admin: true,
        roles: ["authenticated", "administrator"],
        email: "noreply@pdc.wa.gov",
        auth: {
          token: "fake-token",
          expires: 10000
        },
        user: {
          name: "Developer",
          email: "developer@noreply.com"
        },
        rights: {
          manage_jurisdictions: true,
          enter_wapdc_data: true,
          access_wapdc_data: true,
          administer_wapdc_data: true,
          enter_compliance_cases: true
        },
        appInfo: {expires: 10000},
      }
    }
  }
  // If there is a failure, wait and try again
  if (data) {
    Object.assign(user, data)
    if (data?.auth?.token) {
      refreshGatewayTokens(data.auth.token)
    }
    // Set the timeout to renew to half the expiration seconds.
    setTimeout(refresh, 120000)
  }
  else {
    setTimeout(refresh,  2000)
  }
}

function userHasRight(right) {
  return user && user.rights && user.rights[right]
}

function userHasPermission(permission) {
  return user &&
    (
      (!!user.permissions?.find(e => e === permission))
      || (!!user.roles?.find(e => e === 'administrator'))
    )
}

function hasAccessToContent(content) {
    return content.limitTo?.(user) ??
      (content.field_access?.[0]?.value && userHasRight(content.field_access[0].value)) ??
      true;
}



