import similarity from "similarity"
import moment from 'moment'
/**
 * Determines similarity between query and text
 * Priority is given in this order:
 *   - exact matches with regard to position,
 *   - exact matches
 *   - similarity
 * @param {string} query
 * @param {string} text
 * @returns {{score: number, match: boolean}}
 */
export const isSimilar = (query, text) => {
  let score = 0.0;
  let match = false;
  const queryLower = query.toLowerCase();
  const textLower = text.toLowerCase();

  if (query.length > 0) {
    if (textLower === queryLower) {
      score = 100;
    } else if (textLower.startsWith(queryLower)) {
      score = 90;
    } else {
      score = similarity(queryLower, textLower) * 50;
      if (textLower.includes(queryLower)) {
        score += 20;
      }
    }
    match = score > 20;
  }
  return { score, match };
};
export function formatDate(date_param) {
  if (!date_param) {
    return ''
  }
  if (moment(date_param, 'YYYY-MM-DD', true).isValid()) {
    return (new moment(date_param, 'YYYY-MM-DD')).format('MM/DD/YYYY')
  }
  else {
    return (new moment(date_param)).format('MM/DD/YYYY')
  }
}

export function formatDateTime(date_param) {
  if (!date_param) {
    return ''
  }
  // If we don't have enough data to represent time, fall back to date display
  if (moment(date_param, 'YYYY-MM-DD', true).isValid()) {
    return (new moment(date_param, 'YYYY-MM-DD')).format('MM/DD/YYYY')
  }
  else {
    return (new moment(date_param)).format('MM/DD/YYYY h:mma')
  }
}


const {format: moneyFormat} = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  maximumFractionDigits: 2
});

export function formatMoney(amount, defaultValue='$0.00') {
  try {
    if(typeof amount !== "undefined" && amount && !isNaN(amount)) {
      return addParenthesis(moneyFormat(amount))
    } else {
      return defaultValue
    }
  } catch(e) {
    console.error(e);
  }
}

function addParenthesis(val) {
  const temp = val.replace('$', '')
  if( temp < 0 ){
    return "$(" + temp + ")"
  } else {
    return val
  }
}

export function formatPhone(phoneNumber) {
  if (!phoneNumber) return;
  // Match against extension formats like 'ext1234', 'ext 1234', 'x1234', 'x 1234'
  const extMatch = phoneNumber.match(/(ext|x)\s*(\d+)/i);
  let extension = "";

  if (extMatch) {
    // Standardize the extension to 'x1234' format
    extension = ` x${extMatch[2]}`;
    // Remove the original extension from the phone number string
    phoneNumber = phoneNumber.replace(extMatch[0], '');
  }

  // Remove non-digit characters
  const cleaned = ('' + phoneNumber).replace(/\D/g, '');

  // Match the remaining phone number against the pattern
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
    return `(${match[1]}) ${match[2]}-${match[3]}${extension}`;
  }
  return phoneNumber;
}
