import {defineConfig} from "vite";
export default defineConfig({
  build: {
    lib: {
      entry:  {
        util: "./lib/util.js",
        rules: "./lib/rules.js",
        application: "./lib/Application.js",
        GatewayDataService: "./lib/GatewayDataService.js",
        ApolloDataService: "./lib/ApolloDataService.js"
      }
    },
    rollupOptions: {
      external: ["vue", "quasar"]
    }
  }
});