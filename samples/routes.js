
const routes = [
  {
    path: '/',
    component: () => import('@wapdc/quasar-app-extension-pdc-ui/src/components/PUserLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
