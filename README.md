# pdc-ui

Project containing general UI libraries for use in all pdc applications.

## Components contained in this library.

## Creating a new quasar application from scratch

First create a new quasar application using the npm init command. 

```shell
npm init quasar
```

You will be prompted for several options when installing: 

- Create the application with the Quasar cli
- For version, choose Quasar 2 with Vue 3 (latest and greatest)
- For script type, use Javascript (we don't typically use typescript)
- For quasar app cli variant, choose Quasar App CLI with Vite
- For CSS pre-processor, choose Sass with SCSS syntax
- For Features, we only need ESLint.  Note that Axios will be added when you add the pdc-ui library. 
- When prompted for eslint settings, choose Prettier

## Adding this library to a quasar application. 

First ensure that the project is pointed at the public disclosure commissions npm registry by adding the following line 
to the .npmrc file that resides in the same directory as package.json: 

```text
@wapdc:registry=https://gitlab.com/api/v4/groups/288013/-/packages/npm/
```

Quasar application extensions are added to an application using the quasar command line tool. **If you do not already 
have the quasar command line tool installed**, install it using the following: 

```shell
npm i -g @quasar/cli
```

After the tool is installed you can add this library to any quasar application using the following: 

```shell
quasar ext add @wapdc/pdc-ui
```

## Using the component library in a new project

The following configuration tasks should be done one time to set up the project so it works like our other applications.

- Replace the contents of src/css/quasar.variables.scss with [samples/quasar.variables.css](samples/quasar.variables.scss) 
- Change the App.vue so that it matches [samples/App.vue](samples/App.vue)
- In .eslintrc.js change the parserOptions ecmaVersion to be 2022 in order to support using async functions in setup files.
- Update the devServer section of quasar.config.js so it matches the sample below
- Update the plugins section of quasar.config.js to include both loading and notify plugins as indicated below.  
- Change the mainLayout reference to use PUserLayout.vue as described in [samples/routes.js](samples/routes.js)

**Sample quasar configuration**
```javascript
   devServer: {
      // change the port number below so it doesn't conflict with other projects
      port: 8910,
      host: "localhost",
      open: true // opens browser window automatically
    }
  ....  
  plugins: ['Loading', 'Notify']  
```

## Adding new components to this library

New components may be added to the component library using the following procedure:

- Create the new component in the src/components folder of this project.
- Add an import statement to import the new component in [src/boot/registerComponents.js](quasar-app-extension-pdc-ui/src/boot/registerComponents.js)

Once the new component is added you need to make sure that you've updated any project that needs the new component in
your dev environments.

```shell
npm update @wapdc/quasar-app-extension-pdc-ui
npm update @wapdc/pdc-ui
```
